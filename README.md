# Using a Recurrent Neural Network and Articulatory Synthesis to Accurately Model Speech Output
Research project by Salvatore Skare and faculty mentor Allison Sauppe, Computer Science Department, University of Wisconsin–La Crosse

## Contents
* [paper](paper/RNN Articulatory Synthesis.pdf) - publication from the 2019 MICS Conference proceedings. Contains details about the research and results.
* [articulatory-synth](articulatory-synth) - The auticulatory synthesizer
* [formant-synth](formant-synth) - The formant synthesizer
* [LibriSpeech_formatter.sh](LibriSpeech_formatter.sh) - A shell script to produce an input file from the LibriSpeech corpus of speech data

## Prerequisites 
* A copy of [eng_to_ipa](https://github.com/mphilli/English-to-IPA).
* [Cython](https://cython.org/)
* [pysptk](https://pypi.org/project/pysptk/)
* [SoundFile](https://pypi.org/project/SoundFile/) and [sounddevice](https://pypi.org/project/sounddevice/) (for playback).
* Tensorflow, Keras, and Numpy libraries

## License 
This research is licensed under the GNU General Public License v3.0 [Learn more](LISCENCE).

## Acknowledgments
This research was made possible by by the University Wisconsin — La Crosse Dean's Distinguished Fellow's grant, as well and funding from the  University Wisconsin — La Crosse McNair Scholars program. Equitment used in this reserach was provided by the University Wisconsin — La Crosse Computer Science Department.
