# vim: set fileencoding=utf-8 :
import tensorflow as tf
import tensorflow.contrib.layers as layers
import numpy as np
import math
import multiprocessing as mp
from phones import ipa_nums, smallMap
import synth

# Input mapping
ipa_map = smallMap()

#RNN variables
INPUT_SIZE    = 3 * len(ipa_map)
DROPOUT_RATE  = 0.2                # Number of nuerons to drop
RNN_HIDDEN    = 50                 # Number of cells per LSTM
RNN_LAYERS    = 6                  # Layers of stacked LSTMs
NN_HIDDEN     = 200
NN_LAYERS     = 20
OUTPUT_SIZE   = 17                 # Outputs for the synth
PADDING_SIZE  = 960000             # 60 secs of 16 kHz audio

SAMPLE_RATE = synth.Synth().sampleRate()
MULTIPLIER  = 5.0
N_SECTS     = 44

def de_normal(inputs):
    outputs = np.empty([len(inputs)])
    #            time, freq, tense, q1,    q2,    lc,   ac,   rc,   sc,    mc,    pg,    lg,   rg,    pm,    lm,    rm,    anp
    abs_range = [True, True, True, False, False, True, True, True, False, False, False, True, False, False, False, False, False]
    max_range = [1.0, 255.0, MULTIPLIER, MULTIPLIER, MULTIPLIER, MULTIPLIER, N_SECTS, N_SECTS, MULTIPLIER, MULTIPLIER, MULTIPLIER, N_SECTS, N_SECTS, MULTIPLIER, N_SECTS, N_SECTS, MULTIPLIER]
    for i in range(len(inputs)):
        mapped = inputs[i]
        if(abs_range[i]):
            mapped = (mapped + 1) / 2.0
        mapped *= max_range[i]
        outputs[i] = mapped
    return outputs

def input_pop(phones, out, j):
    try:
        last = ipa_map.index(int(phones[j-1]))
    except:
        last = 0
    try:
        next = ipa_map.index(int(phones[j+1]))
    except:
        next = 0
    try:
        curr = ipa_map.index(int(phones[j]))
    except:
        curr = 0
    out[curr] = 1.0
    out[len(ipa_map) + last] = 1.0
    out[(2 * len(ipa_map)) + next] = 1.0
    return out    

def wave_from_inputs(wave_inputs):
    max_time = PADDING_SIZE / SAMPLE_RATE
    wave_generator = synth.Synth()
    seconds = 0
    for j in range(wave_inputs.shape[0]):
        if(seconds >= max_time): break
        inputs = de_normal(wave_inputs[j][:])
        seconds += inputs[0]
        wave_generator.createWave(np.clip(inputs[0], 0, max_time), inputs[1:])
    output = np.nan_to_num(wave_generator.getData())[:PADDING_SIZE]
    #normalize the wave
    output -= np.min(output)
    output /= np.ptp(output)
    return np.pad(output, (0, PADDING_SIZE - len(output)), 'constant')

def wave_gen(wave_inputs):
    batch_size = len(wave_inputs)
    wave_outputs = np.zeros((batch_size, PADDING_SIZE))
    pool = mp.Pool(processes=batch_size)
    wave_outputs[:] = pool.map(wave_from_inputs, wave_inputs)
    pool.close()
    return wave_outputs.astype(np.float32)

def wave_gen_grad_impl(x, y):
    # Set the derivitives all to one
    grad = np.ones(x.shape, dtype=np.float32)
    return grad

def wave_gen_grad(op, grad):
    return tf.py_func(wave_gen_grad_impl, [op.inputs[0], grad], grad.dtype)

# Define placeholder Tensors
inputs  = tf.placeholder(tf.float32, (None, None, INPUT_SIZE), name='inputs')   # (batch, time, in)
outputs = tf.placeholder(tf.float32, (None, PADDING_SIZE)) # (batch, out)

# Add Dense layers to model
dense = tf.layers.dense(inputs=inputs, units=NN_HIDDEN, activation=tf.nn.relu)

for _ in range(int(math.floor(NN_LAYERS/2))):
    dense = tf.layers.dense(inputs=dense, units=NN_HIDDEN, activation=tf.nn.relu)

# Add LSTM cells to model
cell = tf.contrib.rnn.MultiRNNCell([tf.contrib.rnn.LSTMCell(RNN_HIDDEN, state_is_tuple=True) for _ in range(RNN_LAYERS)], state_is_tuple=True)

# Initialize LSTM cells
batch_size = tf.shape(inputs)[0]
initial_state = cell.zero_state(batch_size, tf.float32)
lstm, rnn_states = tf.nn.dynamic_rnn(cell, dense, initial_state=initial_state)

# Add more Dense layers to model (these with sigmoids)
dense = tf.layers.dense(inputs=lstm, units=int(math.ceil(NN_HIDDEN/2)), activation=tf.nn.sigmoid)

for _ in range(int(math.ceil(NN_LAYERS/2))):
    dense = tf.layers.dense(inputs=dense, units=int(math.ceil(NN_HIDDEN/2)), activation=tf.nn.sigmoid)

# Add droput layer
rnn_outputs = tf.layers.dropout(dense, rate=DROPOUT_RATE)

# Resize output from LSTMs and apply activation function
final_projection = lambda x: layers.linear(x, num_outputs=OUTPUT_SIZE, activation_fn=tf.nn.sigmoid)
predicted_outputs = tf.map_fn(final_projection, rnn_outputs)

# Add the synthesizer layer
with tf.name_scope("VoiceLayer", "VoiceLayer", [predicted_outputs]) as name:
    # custom gradient op name shouldn't conflict with any other TF op name
    unique_name = 'VoiceSynth'
    tf.RegisterGradient(unique_name)(wave_gen_grad)

    g = tf.get_default_graph()

    # context manager used to override gradients for nodes created in its block
    with g.gradient_override_map({"PyFunc": unique_name}):
        wave_outputs = tf.py_func(wave_gen, [predicted_outputs], [tf.float32], stateful=True, name=name)[0]
        wave_outputs.set_shape((None, PADDING_SIZE))
