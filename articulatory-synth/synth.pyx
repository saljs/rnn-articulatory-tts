cimport cython
import math
import noise
import numpy as np
from config import *

try:
    import sounddevice as sd
    has_sound = True
except ImportError:
    has_sound = False

# Derived from code written by Neil Thapen
# Copyright 2017 Neil Thapen
# https://dood.al/pinktrombone/
#
# Permission is hereby granted, free of charge, to any person obtaining a 
# copy of this software and associated documentation files (the "Software"), 
# to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, 
# and/or sell copies of the Software, and to permit persons to whom the 
# Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in 
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
# IN THE SOFTWARE.

class Synth:
    'Provides a high-level interface to interact with underlying mechanical system'

    def __init__(self):
        self.duration = 0
        self.sampleCount = 1
        self.stack = []
        self.glottis = False
        self.tract = False
        self.framesAvail = False

    class Wave:
        def __init__(self, duration, args):
            self.duration = duration
            self.args = args

    def createWave(self, duration, args):
        self.stack.append(self.Wave(duration, args))
        
    def popWave(self):
        if(not self.stack) : return False
        wave = self.stack.pop(0)
        self.duration = wave.duration
        self.sampleCount = 0
        if(not self.glottis) : self.glottis = Glottis(wave.args[0], wave.args[1])
        else : 
            self.glottis.setFrequency(wave.args[0])
            self.glottis.setTenseness(wave.args[1])
        if(not self.tract) : self.tract = Tract(wave.args[2:])
        else : self.tract.setParams(wave.args[2:])
        return True

    def getOutput(self, i):
        lambda1 = i/blockSize
        lambda2 = (i+0.5)/blockSize
        glottalOutput = self.glottis.runStep(lambda1)
        vocalOutput = 0
        self.tract.runStep(glottalOutput, lambda1)
        vocalOutput += self.tract.getOutput()
        self.tract.runStep(glottalOutput, lambda2)
        vocalOutput += self.tract.getOutput()
        return vocalOutput

    def getBlock(self):
        if (len(self.stack) == 0):
            return np.array([False])
        block_np = np.empty([blockSize], dtype=np.float32)
        cdef float [:] block = block_np
        i = 0
        while i < blockSize:
            if(self.sampleCount < self.duration * sampleRate):
                block[i] = self.getOutput(i)
                self.sampleCount += 1
            elif(self.popWave()):
                block[i] = self.getOutput(i)
                self.sampleCount += 1
            else : block[i] = 0
            i += 1
        self.glottis.finishBlock()
        self.tract.finishBlock()
        return block_np

    def getBlockCount(self):
        samples = (self.duration * sampleRate) - self.sampleCount
        for wave in self.stack:
            samples += wave.duration * sampleRate
        buff = 0
        while buff < samples:
            buff += blockSize
        return buff

    def getData(self):
        #Return the data in the buffer as a numpy array
        y = []
        block = self.getBlock()
        while block.any():
            y.append(block)
            block = self.getBlock()
        return np.reshape(y, len(y)*blockSize)

    def play(self):
        if(not has_sound): return False
        self.framesAvail = True
        with sd.OutputStream(samplerate=sampleRate,
                             blocksize=blockSize, callback=self.playCallback,
                             channels=1, clip_off=True, dither_off=True, 
                             latency='low'):
            while self.framesAvail:
                sd.sleep(1) #so as to not eat up CPU

    def playCallback(self, outdata, frames, time, status):
        block = self.getBlock()
        if(not block.any()):
            self.framesAvail = False
        else:
            outdata[:, 0] = block

    def sampleRate(self):
        return sampleRate

cdef class Glottis:
    'Defines the Glottal wave for the voice synthesizer.'
    
    cdef double timeInWaveform, waveformLength, totalTime
    
    cdef double oldFrequency, newFrequency, smoothFrequency, frequency
    
    cdef double oldTenseness, newTenseness, tenseness, loudness
    cdef double Rd, alpha, E0, epsilon, shift, Delta, Te, omega
    
    def __init__(self, double frequency, double tenseness):
        self.oldFrequency = frequency
        self.newFrequency = frequency
        self.smoothFrequency = frequency
        self.frequency = frequency
        self.timeInWaveform = 0
        self.totalTime = 0
        self.oldTenseness = tenseness
        self.newTenseness = tenseness
        self.tenseness = tenseness
        self.setupWaveform(0)
        
    def setFrequency(self, double frequency):
        self.newFrequency = frequency

    def setTenseness(self, double tenseness):
        self.newTenseness = tenseness
        
    cpdef double runStep(self, double l):
        cdef double timeStep = 1.0 / sampleRate
        self.timeInWaveform += timeStep
        self.totalTime += timeStep
        if (self.timeInWaveform > self.waveformLength):
            self.timeInWaveform -= self.waveformLength
            self.setupWaveform(l)
        
        cdef double out = self.normalizedLFWaveform(self.timeInWaveform/self.waveformLength)
        aspiration = (1-math.sqrt(abs(self.tenseness)))*self.getNoiseModulator()
        aspiration *= 0.2 + 0.02*noise.pnoise1(self.totalTime * 1.99)
        out += aspiration
        return out

    cdef double getNoiseModulator(self):
        cdef double voiced = 0.1+0.2*max(0,math.sin(math.pi*2*self.timeInWaveform/self.waveformLength))
        return self.tenseness * voiced + (1-self.tenseness) * 0.3
        
    cpdef void finishBlock(self):
        cdef double vibrato = vibratoAmount * math.sin(2*math.pi * self.totalTime * vibratoFrequency)
        vibrato += 0.02 * noise.pnoise1(self.totalTime * 4.07)
        vibrato += 0.04 * noise.pnoise1(self.totalTime * 2.15)
        if (self.frequency > self.smoothFrequency):
            self.smoothFrequency = min(self.smoothFrequency * 1.1, self.frequency)
        if (self.frequency < self.smoothFrequency):
            self.smoothFrequency = max(self.smoothFrequency / 1.1, self.frequency)
        self.oldFrequency = self.newFrequency
        self.newFrequency = self.smoothFrequency * (1+vibrato)
        self.oldTenseness = self.newTenseness
        self.newTenseness = self.tenseness + 0.1*noise.pnoise1(self.totalTime*0.46)+0.05*noise.pnoise1(self.totalTime*0.36)
            
    cdef void setupWaveform(self, double l):
        self.frequency = self.oldFrequency*(1-l) + self.newFrequency*l
        self.tenseness = self.oldTenseness*(1-l) + self.newTenseness*l
        self.loudness = math.pow(self.tenseness, 0.25)
        
        self.Rd = 3*(1-self.tenseness)
        self.waveformLength = 1.0/self.frequency
        
        Rd = self.Rd
        if (Rd < 0.5): Rd = 0.5
        if (Rd > 2.7): Rd = 2.7
        #normalized to time = 1, Ee = 1
        Ra = -0.01 + 0.048*Rd
        Rk = 0.224 + 0.118*Rd
        Rg = (Rk/4)*(0.5+1.2*Rk)/(0.11*Rd-Ra*(0.5+1.2*Rk))
        
        Ta = Ra
        Tp = 1 / (2*Rg)
        Te = Tp + Tp*Rk
        
        epsilon = 1/Ta
        shift = np.exp(-epsilon * (1-Te))
        Delta = 1 - shift #divide by this to scale RHS
        
        RHSIntegral = (1/epsilon)*(shift - 1) + (1-Te)*shift
        RHSIntegral = RHSIntegral/Delta
        
        totalLowerIntegral = - (Te-Tp)/2 + RHSIntegral
        totalUpperIntegral = -totalLowerIntegral
        
        omega = math.pi/Tp
        s = math.sin(omega*Te)
        # need E0*e^(alpha*Te)*s = -1 (to meet the return at -1)
        # and E0*e^(alpha*Tp/2) * Tp*2/pi = totalUpperIntegral 
        #             (our approximation of the integral up to Tp)
        # writing x for e^alpha,
        # have E0*x^Te*s = -1 and E0 * x^(Tp/2) * Tp*2/pi = totalUpperIntegral
        # dividing the second by the first,
        # letting y = x^(Tp/2 - Te),
        # y * Tp*2 / (pi*s) = -totalUpperIntegral;
        y = -math.pi*s*totalUpperIntegral / (Tp*2)
        z = np.log(y)
        alpha = z/(Tp/2 - Te)
        E0 = -1 / (s*np.exp(alpha*Te))
        self.alpha = alpha
        self.E0 = E0
        self.epsilon = epsilon
        self.shift = shift
        self.Delta = Delta
        self.Te = Te
        self.omega = omega
        
    cdef double normalizedLFWaveform(self, double t):
        if (t > self.Te):
            output = (-np.exp(-self.epsilon * (t-self.Te)) + self.shift)/self.Delta;
        else:
            output = self.E0 * np.exp(self.alpha*t) * math.sin(self.omega * t);
        return output * self.loudness

cdef class Tract:
    'Defines the area functions for the vocal tract'

    A_np  = np.zeros([n])
    An_np = np.zeros([noseLength])
   
    cdef double [:] A
    cdef double [:] An
    
    R_np                   = np.zeros([n])
    L_np                   = np.zeros([n])
    reflection_np          = np.zeros([n+1], dtype=np.float64)
    newReflection_np       = np.zeros([n+1], dtype=np.float64)
    junctionOutputR_np     = np.zeros([n+1], dtype=np.float64)
    junctionOutputL_np     = np.zeros([n+1], dtype=np.float64)
    noseR_np               = np.zeros([noseLength], dtype=np.float64)
    noseL_np               = np.zeros([noseLength], dtype=np.float64)
    noseJunctionOutputR_np = np.zeros([noseLength+1], dtype=np.float64)
    noseJunctionOutputL_np = np.zeros([noseLength+1], dtype=np.float64)
    noseReflection_np      = np.zeros([noseLength+1], dtype=np.float64)

    cdef double [:] R                   
    cdef double [:] L                   
    cdef double [:] reflection          
    cdef double [:] newReflection       
    cdef double [:] junctionOutputR     
    cdef double [:] junctionOutputL     
    cdef double [:] noseR               
    cdef double [:] noseL               
    cdef double [:] noseJunctionOutputR 
    cdef double [:] noseJunctionOutputL 
    cdef double [:] noseReflection      
                                               
    cdef double reflectionLeft, reflectionRight, reflectionNose
    cdef double newReflectionLeft, newReflectionRight, newReflectionNose
    cdef double lipOutput, noseOutput
    
    cdef double q1, q2, ac, sc, mc, pg, pm, anp
    cdef int lc, rc, lg, rg, lm, rm

    def __init__(self, args):
        self.A                   = self.A_np
        self.An                  = self.An_np
        self.R                   = self.R_np                  
        self.L                   = self.L_np                  
        self.reflection          = self.reflection_np         
        self.newReflection       = self.newReflection_np      
        self.junctionOutputR     = self.junctionOutputR_np    
        self.junctionOutputL     = self.junctionOutputL_np    
        self.noseR               = self.noseR_np              
        self.noseL               = self.noseL_np              
        self.noseJunctionOutputR = self.noseJunctionOutputR_np
        self.noseJunctionOutputL = self.noseJunctionOutputL_np
        self.noseReflection      = self.noseReflection_np     
        self.newReflectionLeft   = 0 
        self.newReflectionRight  = 0
        self.newReflectionNose   = 0 
        self.lipOutput           = 0         
        self.noseOutput          = 0        

        self.setParams(args)
        self.calculateReflections()                
        self.calculateNoseReflections()            
       
    def setParams(self, args):                     
        self.q1 = args[0]                          
        self.q2 = args[1]
        self.lc = int(np.clip(args[2], 0, n))
        self.ac = args[3]
        self.rc = int(np.clip(args[4], 0, n))
        self.sc = args[5]
        self.mc = args[6]
        self.pg = args[7]
        self.lg = int(np.clip(args[8], 0, n))
        self.rg = int(np.clip(args[9], 0, n))
        self.pm = args[10]
        self.lm = int(np.clip(args[11], 0, n))
        self.rm = int(np.clip(args[12], 0, n))
        self.anp = args[13]
        for i in range(n):
            self.A[i] = self.Area(i)
        for i in range(noseLength):
            self.An[i] = self.noseArea(i)
    
    cdef double V(self, int i):
        #Calculate volume of vocal tract segment
        return (math.pi/4) * pow(Omega[i] + (self.q1 * Phi1[i]) + (self.q2 * Phi2[i]), 2)

    cdef double C(self, i):
        #calculate diameter of vocal tract segment
        rcx = self.rc / (1 + self.sc)
        if(i > self.lc):
            rcx *= self.sc
        if(rcx == 0):
            rcx += 0.000001
        out = 1 - (self.mc * (1 - self.ac/self.V(self.lc)) * np.exp(-np.log(16) * pow((i-self.lc)/rcx, 2)))
        return max(0, out)

    cdef double Area(self, int i):
        #calculate area of vocal tract segment
        return self.V(i) * self.C(i)
        
    cdef double noseArea(self, int j):
        #calculate are of nose segment
        if(j == 0) : return N(j) * self.anp
        return N(j)

    def calculateReflections(self):
        for i in range(1, n):
            self.reflection[i] = self.newReflection[i]
            if (self.A[i] == 0) : self.newReflection[i] = 0.999 #to prevent some bad behaviour if 0
            else : self.newReflection[i] = (self.A[i-1]-self.A[i]) / (self.A[i-1]+self.A[i])
        #now at junction with nose
        self.reflectionLeft = self.newReflectionLeft
        self.reflectionRight = self.newReflectionRight
        self.reflectionNose = self.newReflectionNose
        cdef double s = self.A[noseStart]+self.A[noseStart+1]+self.An[0]
        self.newReflectionLeft = (2*self.A[noseStart]-s)/s
        self.newReflectionRight = (2*self.A[noseStart+1]-s)/s
        self.newReflectionNose = (2*self.An[0]-s)/s

    def calculateNoseReflections(self):
        for i in range(1, noseLength):
            if (self.An[i] == 0) : self.noseReflection[i] = 0.999 #to prevent some bad behaviour if 0
            else : self.noseReflection[i] = (self.An[i-1]-self.An[i]) / (self.An[i-1]+self.An[i])

    cpdef double getOutput(self):
        return self.lipOutput + self.noseOutput

    cpdef double runStep(self, double glottalOutput, double l):
        #mouth
        self.junctionOutputR[0] = self.L[0] * glottalReflection + glottalOutput
        self.junctionOutputL[n] = self.R[n-1] * lipReflection
        #now at junction with nose
        cdef int j = noseStart
        cdef double r = self.newReflectionLeft * (1-l) + self.reflectionLeft*l
        self.junctionOutputL[j] = r*self.R[j-1]+(1+r)*(self.noseL[0]+self.L[j])
        r = self.newReflectionRight * (1-l) + self.reflectionRight*l
        self.junctionOutputR[j] = r*self.L[j]+(1+r)*(self.R[j-1]+self.noseL[0])
        r = self.newReflectionNose * (1-l) + self.reflectionNose*l
        self.noseJunctionOutputR[0] = r*self.noseL[0]+(1+r)*(self.L[j]+self.R[j-1])
        #nose
        self.noseJunctionOutputL[noseLength] = self.noseR[noseLength-1] * lipReflection
        for i in range(1, n):
            r = self.reflection[i] * (1-l) + self.newReflection[i]*l
            w = r * (self.R[i-1] + self.L[i])
            self.junctionOutputR[i] = self.R[i-1] - w
            self.junctionOutputL[i] = self.L[i] + w
            if(i < noseLength):
                w = self.noseReflection[i] * (self.noseR[i-1] + self.noseL[i])
                self.noseJunctionOutputR[i] = self.noseR[i-1] - w
                self.noseJunctionOutputL[i] = self.noseL[i] + w

        for i in range(n):
            self.R[i] = self.junctionOutputR[i]*0.999
            self.L[i] = self.junctionOutputL[i+1]*0.999
            if(i < noseLength): 
                self.noseR[i] = self.noseJunctionOutputR[i] * fade
                self.noseL[i] = self.noseJunctionOutputL[i+1] * fade
        
        self.noseOutput = self.noseR[noseLength-1]
        self.lipOutput = self.R[n-1]

    def finishBlock(self):
        for i in range(n):
            self.A[i] = self.Area(i)
        for i in range(noseLength):
            self.An[i] = self.noseArea(i)
        self.calculateReflections()
        self.calculateNoseReflections()
