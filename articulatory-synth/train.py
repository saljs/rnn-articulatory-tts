#!/bin/env python
# vim: set fileencoding=utf-8 :

import tensorflow as tf
import numpy as np
import argparse, math
import os, sys
from progress.bar import ShadyBar
import soundfile as sf
from rnn import *

# Training variables
TIME_STEPS    = 1000
EPOCHS        = 2
LEARN_RATE    = 1e-6
BATCH_SIZE    = 4
GOOD_ERROR    = 0.01

# Arguments
parser = argparse.ArgumentParser(description="Train the TTS neural network on a set of data")
parser.add_argument('filename', help="File containing training data in the format: [filename text...]")
parser.add_argument('save_file', help="Output file for the saved model")
parser.add_argument('--graph', help="Initialization values for the graph")
parser.add_argument('--epochs', help="Number of epochs (defaults to 2).")
parser.add_argument('--learn', help="Learning rate (defaults to 1e-6).")
parser.add_argument('--batch', help="Batch size. Set to number of cores for best performance.")
args = parser.parse_args()
save_file = args.save_file
data_file = open(args.filename, "r")

data_file.seek(0, 2)
file_length = data_file.tell()
data_file.seek(0)
if(args.epochs):
    EPOCHS = int(args.epochs)
if(args.learn):
    LEARN_RATE = float(args.learn)
if(args.batch):
    BATCH_SIZE = int(args.batch)

# Progress bar
bar = ShadyBar('Training', max=int(math.ceil(file_length * EPOCHS)), suffix='%(percent)d%%')

def generate_batch(batch_size):
    x = np.zeros((batch_size, TIME_STEPS, INPUT_SIZE))
    y = np.zeros((batch_size, PADDING_SIZE))
    batch = 0;
    for i in range(batch_size):
        index = data_file.readline()
        if(len(index) == 0): break
        batch += 1
        wavefile, text = index.split(' ', 1)
        phones = text.split(' ')
        for j in range(TIME_STEPS):
            #set values in Tensor
            x[i][j][:] = input_pop(phones, x[i][j][:], j)
        sound, srate = sf.read(wavefile, dtype='float32')
        #normalize wave
        sound -= np.min(sound)
        sound /= np.ptp(sound)
        y[i][:len(sound)] = sound[:]
    return x[:batch,:,:], y[:batch,:]
    

#error (loss) function
error = tf.losses.mean_squared_error(outputs, wave_outputs)

# optimize function
train_fn = tf.train.AdamOptimizer(learning_rate=LEARN_RATE).minimize(error)

#init the vars
init_op = tf.global_variables_initializer()

#saver object
saver = tf.train.Saver()

########################
#  Training loop       #
########################

session = tf.Session()

if(args.graph):
    saver.restore(session, args.graph)

#log some vars for TensorBoard
tf.summary.scalar('Loss', error)
# Merge all summaries into a single op
merged_summary_op = tf.summary.merge_all()
logdir = save_file + ".logs"
if(not os.path.exists(logdir)):
    os.mkdir(logdir)
log_writer = tf.summary.FileWriter(logdir, session.graph)

session.run(init_op)
bar.next(0)
max_batches = 0;

for epoch in range(EPOCHS):
    batch_num = 0
    last_read = data_file.tell()
    while last_read < file_length:
        x, y = generate_batch(BATCH_SIZE)
        epoch_error, _, summary = session.run(
            [error, train_fn, merged_summary_op], 
            {
                inputs: x,
                outputs: y,
            })
        log_writer.add_summary(summary, (epoch * max_batches) + batch_num)
        if(batch_num > max_batches): max_batches = batch_num
        saver.save(session, save_file)
        batch_num += 1
        bar.next(data_file.tell() - last_read)
        last_read = data_file.tell()
        if( math.isnan(epoch_error) or math.isinf(epoch_error) ):
            print("\nError: Graph does not converge!")
            bar.finish()
            data_file.close()
            exit(1)
        if( epoch_error <= GOOD_ERROR ):
            print("\nFinal error is " + str(epoch_error) + ". Exiting.")
            bar.finish()
            data_file.close()
            exit(0)
    data_file.seek(0)

bar.finish()
data_file.close()
print("\nFinished.")
