# vim: set fileencoding=utf-8 :

from eng_to_ipa import ipa_list, get_all, get_top

ipa_nums = {"p":101, "b":102, "t":103, "d":104, "ʈ":105, "ɖ":106, "c":107, "ɟ":108, "k":109, "g":110, "q":111, "ɢ":112, "ʔ":113, "m":114, "ɱ":115, "n":116, "ɳ":117, "ɲ":118, "ŋ":119, "ɴ":120, "ʙ":121, "r":122, "ʀ":123, "ⱱ":184, "ɾ":124, "ɽ":125, "ɸ":126, "β":127, "f":128, "v":129, "θ":130, "ð":131, "s":132, "z":133, "ʃ":134, "ʒ":135, "ʂ":136, "ʐ":137, "ç":138, "ʝ":139, "x":140, "ɣ":141, "χ":142, "ʁ":143, "ħ":144, "ʕ":145, "h":146, "ɦ":147, "ɬ":148, "ɮ":149, "ʋ":150, "ɹ":151, "ɻ":152, "j":153, "ɰ":154, "l":155, "ɭ":156, "ʎ":157, "ʟ":158, "ʘ":176, "ɓ":160, "ˈ":401, "ˌ":502, "|":177, "ɗ":162, "!":178, "ʄ":164, "ǂ":179, "ɠ":166, "ǁ":180, "ʛ":168, "ʍ":169, "ɕ":182, "ʑ":183, "w":170, "ɺ":181, "ɥ":171, "ɧ":175, "ʜ":172, "ʢ":174, "ʡ":173, "ʤ":[104, 135], "i":301, "y":309, "ɨ":317, "ʉ":318, "ɯ":316, "u":308, "ɪ":319, "ʏ":320, "ʊ":321, "e":302, "ø":310, "ɘ":397, "ɵ":323, "ɤ":315, "o":307, "ə":322, "ɛ":303, "œ":311, "ɜ":326, "ɞ":395, "ʌ":314, "ɔ":306, "æ":325, "ɐ":324, "a":304, "ɶ":312, "ɑ":305, "ɒ":313, " ":0}

def smallMap():
    index, items = zip(*ipa_nums.items())
    num_map = []
    for num in items:
        if(isinstance(num, list)) : num_map.extend(num)
        else : num_map.append(num)
    num_map.sort()
    return num_map

def convert_num(text, lookup_table, retrieve_all=False, stress_marks='both'):
    """takes either a string or list of English words and converts them to IPA numbers"""
    ipa = ipa_list(
                   words_in=text,
                   keep_punct=False,
                   stress_marks=stress_marks
                   )
    codes = []
    if retrieve_all:
        codes = get_all(ipa)
    codes = get_top(ipa)
    mapped = []
    for ch in codes:
        if ch in lookup_table:
            if(isinstance(lookup_table[ch], list)) : mapped.extend(lookup_table[ch])
            else : mapped.append(lookup_table[ch])
    return mapped
