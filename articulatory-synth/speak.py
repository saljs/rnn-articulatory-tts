#!/bin/env python
# vim: set fileencoding=utf-8 :

import tensorflow as tf
import numpy as np
import argparse
import matplotlib.pyplot as plt
import sounddevice as sd
from phones import ipa_nums, convert_num
import synth
from rnn import *

GRAPH = True

# Arguments
parser = argparse.ArgumentParser(description="Speak inputted text using a saved RNN model")
parser.add_argument('import_file', help="Input file for the saved model")
parser.add_argument('text', help="Text to speak")
args = parser.parse_args()

def play(params):
    voice = synth.Synth()
    duration = 0
    for i in range(len(params)):
        inputs = de_normal(params[i])
        voice.createWave(inputs[0], inputs[1:])
        duration += inputs[0]
    print(str(duration) + " seconds of audio.")
    if GRAPH:
        yvals = voice.getData()
        xvals = np.arange(0, len(yvals) / SAMPLE_RATE, 1.0 / SAMPLE_RATE)
        sd.play(yvals, SAMPLE_RATE, blocking=True)
        plt.plot(xvals, yvals)
        plt.show()
    else:
        voice.play()

# Add ops to save and restore all the variables.
saver = tf.train.Saver()

with tf.Session() as session:
    saver.restore(session, args.import_file)

    phones = convert_num(args.text, ipa_nums)
    x = np.zeros((1, len(phones), INPUT_SIZE))
    for j in range(len(phones)):
        x[0][j][:] = input_pop(phones, x[0][j][:], j)
    feed_dict = {inputs: x}
    classification = session.run(predicted_outputs, feed_dict)
    inputs = classification[0]
    play(inputs)
