# vim: set fileencoding=utf-8 :
# Config file for the mechanical components of the speech synthesizer.
# Data taken from Appendix A of:
#   Story, Brad H. "A parametric model of the vocal tract area function for 
#   vowel and consonant simulation." 
#   The Journal of the Acoustical Society of America 117.5 (2005): 3231-3254.
# Author: Sal Skare <skare.salvato@uwlax.edu>
import numpy as np
import math

#Samples per second
sampleRate = 44000
#Number of audio samples per block
blockSize = 512

#used for applying vibrato to glottal wave
vibratoAmount = 0.005
vibratoFrequency = 6

#number of x-sect. areas contained in the vocal-tract area function
n = 44

#mean vocal-tract shape (diameter function)
Omega = np.array([0.636, 0.561, 0.561, 0.550, 0.598, 0.895, 1.187, 1.417, 1.380,
    1.273, 1.340, 1.399, 1.433, 1.506, 1.493, 1.473, 1.499, 1.529, 1.567, 1.601,
    1.591, 1.547, 1.570, 1.546, 1.532, 1.496, 1.429, 1.425, 1.496, 1.608, 1.668,
    1.757, 1.842, 1.983, 2.073, 2.123, 2.194, 2.175, 2.009, 1.785, 1.675, 1.539,
    1.405, 1.312]);

#area function modes (basis functions)
Phi1 = np.array([0.018, 0.001, -0.013, -0.025, -0.036, -0.048, -0.062, -0.076,
    -0.093, -0.111, -0.130, -0.149, -0.167, -0.183, -0.196, -0.204, -0.207,
    -0.203, -0.193, -0.175, -0.151, -0.119, -0.082, -0.041, 0.004, 0.051, 0.097,
    0.141, 0.181, 0.214, 0.240, 0.257, 0.264, 0.260, 0.246, 0.224, 0.194, 0.159,
    0.122, 0.087, 0.057, 0.038, 0.034, 0.048]);
Phi2 = np.array([-0.013, -0.007, -0.029, -0.059, -0.088, -0.108, -0.120, -0.123,
    -0.118, -0.107, -0.092, -0.075, -0.056, -0.035, -0.014, 0.008, 0.032, 0.057,
    0.084, 0.111, 0.138, 0.164, 0.188, 0.206, 0.218, 0.221, 0.214, 0.195, 0.164,
    0.121, 0.070, 0.013, -0.046, -0.100, -0.143, -0.167, -0.165, -0.132, -0.066,
    0.031, 0.148, 0.264, 0.346, 0.338]);

#nominal (default) length vector (length of each tubelet in an area function)
Delta = 0.396825

#Nasal tract area function
def N(j):
    diameter = 0
    d = 2*(j/noseLength)
    if (d < 1) : diameter = 0.4+1.6*d
    else : diameter = 0.5+1.5*(2-d)
    diameter = min(diameter, 1.9);
    return math.pi * pow(diameter/2, 2)

glottalReflection = 0.75
lipReflection = -0.85
fade = 1.0 #0.9999
noseLength = 28
noseStart = n - noseLength + 1
