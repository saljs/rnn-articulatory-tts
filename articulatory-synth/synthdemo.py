#!/bin/env python
# vim: set fileencoding=utf-8 :

import numpy as np
import synth
import time
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
import sounddevice as sd
from rnn import de_normal

fullWave = True

matplotlib.rcParams.update({'font.size': 9})

# Wave variables:
SECONDS = 2
params = np.array([0.54, 0.06, -0.3, -0.2, 0.95, 0.01, 0.09, 0.1,  0.1, -0.1, 0.1, 0.18, 0.2, 0.98, 0.18, 0.0])
lables = ["Frequncy", "Tenseness", "amplitude coefficient 1", "amplitude coefficient 2", "section of constriction", "cross-sectional area of constriction", "range of the consonant superposition", "skewing quotient", "magnitude of the consonant superposition", "length change at the glottal end", "center of glottal length change", "range of the glottal length change", "length change at the lip end", "center of lip length change", "range of the lip length change", "cross-sectional area of nasal coupling port"]

#Synth objects
wave = synth.Synth()
sampleRate = synth.Synth().sampleRate()
output = np.zeros(SECONDS * sampleRate)

# Create UI
fig, ax = plt.subplots()
plt.subplots_adjust(left=0.23, bottom=0.6, top=0.98, right=0.98)
t = np.arange(0.0, len(output)/sampleRate, 1.0/sampleRate)
line, = plt.plot(t, output)

axcolor = 'lightgoldenrodyellow'
sliders = [None] * len(params)

for i in range(len(params)):
    sliders[i] = Slider(plt.axes([0.35, 0.5 - (0.03*i), 0.6, 0.03], facecolor=axcolor), lables[i], -1.0, 1.0, valinit=params[i])

button = Button(plt.axes([0.018, 0.8, 0.1, 0.04]), 'Play', color=axcolor, hovercolor='0.975')
def playSound(event):
    global wave, output, params
    for i in range(len(params)):
        params[i] = sliders[i].val
    inputs = de_normal(np.insert(params, 0, 1.0))[1:]
    if(fullWave):
        wave.createWave(SECONDS, inputs)
        output = wave.getData()
    else:
        glottis = synth.Glottis(inputs[0], inputs[1])
        output = np.zeros(SECONDS * sampleRate)
        for i in range(SECONDS * sampleRate):
            lambda1 = i/(SECONDS * sampleRate)
            output[i] = glottis.runStep(lambda1)
    t = np.arange(0.0, len(output)/sampleRate, 1.0/sampleRate)
    line.set_ydata(output)
    line.set_xdata(t)
    ax.relim()
    ax.autoscale_view()
    fig.canvas.draw_idle()
    sd.play(output, sampleRate, blocking=False)
button.on_clicked(playSound)

radio = RadioButtons(plt.axes([0.018, 0.65, 0.15, 0.1]), ('Vocal tract on', 'Vocal tract off'), active=0)
def updateOpt(label):
    global fullWave
    if(label == "Vocal tract on"):
        fullWave = True
    else :
        fullWave = False
radio.on_clicked(updateOpt)

reset = Button(plt.axes([0.018, 0.6, 0.1, 0.04]), 'Reset', color=axcolor, hovercolor='0.975')
def resetSynth(event):
    global wave, params, sliders
    wave = synth.Synth()
    params = np.array([0.54, 0.06, -0.3, -0.2, 0.95, 0.01, 0.09, 0.1,  0.1, -0.1, 0.1, 0.18, 0.2, 0.98, 0.18, 0.0])
    for i in range(len(sliders)):
        sliders[i].set_val(params[i])
reset.on_clicked(resetSynth)


plt.show()
