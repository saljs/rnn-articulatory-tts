#!/bin/env python
# vim: set fileencoding=utf-8 :

import argparse
import matplotlib.pyplot as plt
import sounddevice as sd
from pysptk.synthesis import Synthesizer, AllPoleDF
from phones import ipa_nums, convert_num
from rnn import *

K.set_session(K.tf.Session(config=K.tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)))


# Arguments
parser = argparse.ArgumentParser(description="Speak inputted text using a saved RNN model")
parser.add_argument('import_file', help="Input file for the saved model")
args = parser.parse_args()

model = TTS_RNN()
model.load_weights(args.import_file)

synthesizer = Synthesizer(AllPoleDF(order=ORDER), HOP_LEN)

text = input('Text: ')
while(text):
    phones = convert_num(text, ipa_nums)
    x = input_gen(phones)
    excite, lpc = model.predict(np.array([x]), batch_size=1)
    excite = np.reshape(excite, (PADDING_SIZE))
    lpc = np.reshape(lpc, (N_FRAMES, ORDER+1))
    
    excite = descale(excite, -1, 20)
    for i in range(ORDER+1):
        lpc[:,i] = descale(lpc[:,i], -20, 20)
    lpc[:, 0][lpc[:, 0] <= 0] = 0.00001
    lpc[:, 0] = np.log(lpc[:, 0])
    
    plt.plot(excite)
    plt.show()
    plt.plot(lpc)
    plt.show()
    
    audio = synthesizer.synthesis(excite.astype('float64'), lpc.astype('float64'))
    plt.plot(audio)
    plt.show()
    sd.play(audio, SAMPLE_RATE, blocking=True)

    text = input('Text: ')
