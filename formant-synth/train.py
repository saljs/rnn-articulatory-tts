#!/bin/env python
# vim: set fileencoding=utf-8 :

import argparse, math
import os, sys

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# KEEP THIS AS 1 UNLESS ALL INPUT SEQUENCES ARE THE SAME LENGTH!
BATCH_SIZE    = 1

# Training variables
EPOCHS        = 100
TEST_PERC     = 0.10
THREADS       = 2
PRELOAD       = 5

# Arguments
parser = argparse.ArgumentParser(description="Train the TTS neural network on a set of data")
parser.add_argument('filename', help="File containing training data in the format: [filename phones...]")
parser.add_argument('save_file', help="Output file for the saved model")
parser.add_argument('--epochs', help="Number of epochs (defaults to 100).")
parser.add_argument('--test', help="Percent of samples to use for testing (defaults to 0.10).")
parser.add_argument('--threads', help="Number of helper threads (defaults to 2).")
parser.add_argument('--preload', help="Number of samples to preload (defaults to 5).")
args = parser.parse_args()

if(args.epochs):
    EPOCHS = int(args.epochs)
if(args.test):
    TEST_PERC = float(args.test)
if(args.threads):
    THREADS = int(args.threads)
if(args.preload):
    THREADS = int(args.preload)

import tensorflow as tf
import soundfile as sf
import pysptk
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.utils import Sequence, print_summary
#get all functions and variables from RNN
from rnn import *

print("Reading training file...", end=" ", flush=True)
training_samples = []
testing_samples = []
with open(args.filename, "r") as f:
    index_content = f.readlines()
for line in index_content:
    wavefile, text = line.split(' ', 1)
    phones = list(map(int, text.split(' ')))
    if(np.random.random() < TEST_PERC):
        testing_samples.append((wavefile, phones))
    else:
        training_samples.append((wavefile, phones))
print("done!")
print("Loaded " + str(len(training_samples)) + " training samples.")
print("Loaded " + str(len(testing_samples)) + " testing samples.")

class AudioSequence(Sequence):
    def __init__(self, data, batch_size):
        self.data = data
        self.batch_size = batch_size
    def __len__(self):
        return int(np.ceil(len(self.data) / float(self.batch_size)))
    def __getitem__(self, idx):
        batch_x = []
        batch_y_excite = []
        batch_y_lpc = []
        for i in range(self.batch_size):
            ret = self.__singleitem(i + idx)
            if(ret):
                x, y = ret
                batch_x.append(x)
                batch_y_excite.append(y[0])
                batch_y_lpc.append(y[1])
        if(len(batch_x) < 1):
            if(idx + 1 < len(self.data)):
                return self.__getitem__(idx + 1)
            else:
                return self.__getitem__(0)
        return np.array(batch_x), [np.array(batch_y_excite), np.array(batch_y_lpc)]
    def __singleitem(self, idx):
        y_excite = np.zeros(PADDING_SIZE)
        y_lpc = np.zeros((N_FRAMES, ORDER+1))
        audio, srate = sf.read(self.data[idx][0])
        if(len(audio) >= PADDING_SIZE or srate != SAMPLE_RATE):
            return False
        n_frames = 1 + int((len(audio) - FRAME_LEN) / HOP_LEN)
        frames = np.lib.stride_tricks.as_strided(audio, shape=(FRAME_LEN, n_frames),
                strides=(audio.itemsize, HOP_LEN * audio.itemsize)).astype(np.float64).T
        frames *= pysptk.blackman(FRAME_LEN)
        try:
            pitch = pysptk.swipe(audio.astype(np.float64), fs=SAMPLE_RATE, hopsize=HOP_LEN, min=60, max=260, otype="pitch")
            lpc = pysptk.lpc(frames, ORDER)
            source_excitation = pysptk.excite(pitch, HOP_LEN)
        except:
            return False
        #Normalize
        y_excite[0:len(source_excitation)] = scale(source_excitation, -1, 20)
        for i in range(ORDER+1):
            y = scale(lpc[:,i], -20, 20)
            y_lpc[:,i][0:len(y)] = y
        x = input_gen(self.data[idx][1])
        return x, [y_excite, y_lpc]

# Set up sequences
train_batch = AudioSequence(training_samples, BATCH_SIZE)
test_batch = AudioSequence(testing_samples, BATCH_SIZE)

# Limit TensorFlow GPU use
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.per_process_gpu_memory_fraction = 0.25
K.tensorflow_backend.set_session(tf.Session(config=config))

#Callbacks
savedir = args.save_file + ".save"
if(not os.path.exists(savedir)):
    os.mkdir(savedir)
save = ModelCheckpoint(savedir + "/{epoch:02d}.hd5f", save_weights_only=True)
logdir = args.save_file + ".logs"
if(not os.path.exists(logdir)):
    os.mkdir(logdir)
plot = TensorBoard(logdir, write_graph=False, update_freq=64)

#Train
print("Loading RNN...", end=" ", flush=True)
model = TTS_RNN()
print("done!")

model.fit_generator(train_batch, 
                    steps_per_epoch=int(len(training_samples)/BATCH_SIZE),
                    epochs = EPOCHS,
                    validation_data = test_batch,
                    workers = THREADS,
                    callbacks = [save, plot],
                    shuffle = True,
                    verbose = 1,
                    max_queue_size = PRELOAD,
                    use_multiprocessing = True)
