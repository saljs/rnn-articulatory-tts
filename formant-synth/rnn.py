# vim: set fileencoding=utf-8 :
import numpy as np
import math
import multiprocessing as mp
import phones

from keras.models import Model
from keras import backend as K
from keras.layers import Input, Dense, Flatten, LSTM, Reshape, Permute, Conv2DTranspose, BatchNormalization, LeakyReLU, Activation

#Input Mapping
ipa_map = phones.smallMap()

#Synth variables
SAMPLE_RATE   = 16000
FRAME_LEN     = 1024
HOP_LEN       = 80
ORDER         = 25
PADDING_SIZE  = SAMPLE_RATE * 10   # 10 seconds of audio
N_FRAMES      = 1 + int((PADDING_SIZE - FRAME_LEN) / HOP_LEN)

#RNN Variables
RNN_LAYERS    = 5
RNN_SIZE      = 256
DROPOUT       = 0.2
#Conv Variables
CONV_KERN     = 5
CONV_LAYERS   = 3


#Data manipulation functions
def scale(x, small, large):
    '''Scales x in range [small, large] to y in range [0, 1]'''
    return (x - small) / (large - small)

def descale(norm, small, large):
    '''Scales x in range [0, 1] to y in range [small, large]'''
    return (norm * (large - small)) + small

def input_gen(phones):
    x = np.zeros((len(phones), len(ipa_map)))
    for i in range(len(phones)):
        try:
            x[i][ipa_map.index(phones[i])] = 1
        except:
            x[i] = 0
    return x

#Shorthand for a deconv layer that takes 1D inputs and generates output 
# of 2D size dims
def Deconv(inputs, dims, kernal):
    outputs = Dense(dims[0])(inputs)
    if K.image_data_format() == 'channels_first':
        outputs = Reshape((1, dims[0], 1))(outputs)
        bn_axis = 1
    else:
        outputs = Reshape((dims[0], 1, 1))(outputs)
        bn_axis = -1
    for i in range(CONV_LAYERS):
        outputs = BatchNormalization(axis=bn_axis)(outputs)
        outputs = LeakyReLU()(outputs)
        outputs = Conv2DTranspose(dims[1], (1, CONV_KERN), padding='same')(outputs)
    outputs = Reshape((dims[1], dims[0]))(outputs)
    outputs = Permute((2, 1))(outputs)
    return outputs

#Model layer
def TTS_RNN():
    #Add input layer
    inputs = Input(shape=(None, len(ipa_map)), dtype='float32')
    
    #Set up placeholders
    excite_nn = inputs
    lpc_nn = inputs
    
    #Add in recurrent layers
    for _ in range(RNN_LAYERS-1):
        excite_nn = LSTM(RNN_SIZE, dropout=DROPOUT, unit_forget_bias=True, return_sequences=True)(excite_nn)
        lpc_nn = LSTM(RNN_SIZE, dropout=DROPOUT, unit_forget_bias=True, return_sequences=True)(lpc_nn)
    excite_nn = LSTM(RNN_SIZE)(excite_nn)
    lpc_nn = LSTM(RNN_SIZE)(lpc_nn)

    #Add in deconv layers for excite
    excite_nn = Deconv(excite_nn, (PADDING_SIZE, 1), CONV_KERN)
    excite_nn = Flatten()(excite_nn)

    #Add in deconv layers for LPC
    lpc_nn = Deconv(lpc_nn, (N_FRAMES, ORDER+1), CONV_KERN)
    
    #Add in output layers
    excite_outputs = Activation('sigmoid', name='excitation')(excite_nn)
    lpc_outputs = Activation('sigmoid', name='lpc')(lpc_nn)
    
    #Compile model
    model = Model(inputs=inputs, outputs=[excite_outputs, lpc_outputs])
    model.compile(optimizer='adadelta', loss='logcosh', metrics=['mse'])
    return model

