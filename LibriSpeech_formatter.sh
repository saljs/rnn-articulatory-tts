#Formats LibriSpeech corpus into a training file suitable for train.py
#!/bin/bash

declare -a index_files
mapfile -t index_files < <( find "$1" -name "*.txt")

for i in "${index_files[@]}"
do
    indexfile=$(basename "$i")
    path=$(echo "$i" | sed "s/$indexfile//")
    cat $i | while read line
    do
        filename=$path$(echo $line | sed 's/ .*$/\.flac /')
        text=$(echo $line | sed 's/[^ ]* //')
        echo -n $filename
        python3 << END
from phones import convert_num, ipa_nums
codes = convert_num("$text", ipa_nums)
for code in codes:
   print(' ' + str(code), end='')
print('')
END
    done
done
